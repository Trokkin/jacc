#include "core.h"
#include "interface.h"
#include "event.h"
#include "waiter.h"
#include "logger.h"
#include <algorithm>

int run;
Session* current_session;
Event<> event_always;
Event<int, int> event_lclick;
function<void()> tick_f = [](){
	current_session->time++;
	event_always.call();
	Waiter::add(16, tick_f);
	mlog.msg(TRACE, "a tick of %d", current_session->time);
};

void create_session(){
	run=1;
	if(current_session != nullptr) delete current_session;
	current_session = new Session();
	current_session->map.hexes = new Hex*[20]();
	for(int i = 0; i < 20; i++) current_session->map.hexes[i] = new Hex[20]();
	Waiter::add(16, tick_f);
}

void close_session(){
	for(int i = 0; i < 20; i++) delete[] current_session->map.hexes[i];
	delete[] current_session->map.hexes;
	delete current_session;
}