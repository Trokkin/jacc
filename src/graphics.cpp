#include "graphics.h"
#include "core.h"
#include "event.h"

SDL_Renderer *renderer;
SDL_Window *window;

Manager manager;

Sprite s;
Event<>::Listener* redraw_listener;
void init_graphics() {
	SDL_Init(SDL_INIT_VIDEO);
	IMG_Init(0);
	window = SDL_CreateWindow("test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIN_W, WIN_H, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
	
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	redraw_listener = event_always.listen([](){
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, s.tex, 0, 0);
		SDL_RenderPresent(renderer);
	});
	s=Sprite("assets/desert.png");
}

void freeGraphics() {
	event_always.unlisten(redraw_listener);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit();
	SDL_Quit();
}

Sprite::Sprite(char *path) {
	SDL_Surface *surf = IMG_Load(path);
	SDL_SetColorKey(surf, SDL_TRUE, SDL_MapRGB(surf->format, 255, 255, 255));
	tex=SDL_CreateTextureFromSurface(renderer, surf);
	SDL_FreeSurface(surf);
	manager.textures.push_back(tex);
}