#include "logger.h"

#include <cstdio>
#include <vector>
#include <iostream>
#include <cstdio>

Log mlog("log.txt", TRACE);

Log::Log(const std::string& filename) {
    level_ = INFO;
    stream_ = fopen(filename.c_str(), "w");
    msg("Log opened successfully.");
}
Log::Log(const std::string& filename, LogLevel level) {
    level_ = level;
    stream_ = fopen(filename.c_str(), "w");
    msg("Log opened successfully.");
}

std::string format_(const char *fmt, va_list args) {
    std::vector<char> v(1024);
    while (true) {
        va_list args2;
        va_copy(args2, args);
        int res = vsnprintf(v.data(), v.size(), fmt, args2);
        if ((res >= 0) && (res < static_cast<int>(v.size()))) {
            va_end(args);
            va_end(args2);
            return std::string(v.data());
        }
        size_t size;
        if (res < 0)
            size = v.size() * 2;
        else
            size = static_cast<size_t>(res) + 1;
        v.clear();
        v.resize(size);
        va_end(args2);
    }
}

void Log::print(const char* fmt, va_list val) {
	mtx_.lock();
    fprintf(stream_, "> [%s : %ld] ", watch_.getTimeString().c_str(), watch_.stop_watch() / 1000000);
    vfprintf(stream_, fmt, val);
    fprintf(stream_, "\n");
	if(redirect_) {
        printf("> [%s : %ld] ", watch_.getTimeString().c_str(), watch_.stop_watch() / 1000000);
        vprintf(fmt, val);
        printf("\n");
	}
	mtx_.unlock();
}

void Log::msg(LogLevel level, const char *format_, ...) {
    if(level >= level_) {
	    va_list args;
	    va_start(args, format_);
		print(format_, args);
        va_end(args);
	}
}
void Log::msg(const char *format_, ... ) {
    va_list args;
    va_start(args, format_);
	print(format_, args);
    va_end(args);
}

std::string format(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    std::vector<char> v(1024);
    while (true) {
        va_list args2;
        va_copy(args2, args);
        int res = vsnprintf(v.data(), v.size(), fmt, args2);
        if ((res >= 0) && (res < static_cast<int>(v.size()))) {
            va_end(args);
            va_end(args2);
            return std::string(v.data());
        }
        size_t size;
        if (res < 0)
            size = v.size() * 2;
        else
            size = static_cast<size_t>(res) + 1;
        v.clear();
        v.resize(size);
        va_end(args2);
    }
}