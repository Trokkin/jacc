#include "interface.h"

#include <SDL2/SDL.h>

#include "core.h"
#include "waiter.h"

using namespace std;

int mouse_x, mouse_y;

void catch_event(){
	SDL_Event e;
	Waiter::check();
	while(SDL_PollEvent(&e)){
		switch(e.type){
			case SDL_QUIT:
				run=0;
				break;
			case SDL_MOUSEBUTTONDOWN:
				if(e.button.button==SDL_BUTTON_LEFT && e.button.clicks)
					event_lclick.call(mouse_x, mouse_y);
				break;
			case SDL_MOUSEMOTION:
				mouse_x = e.motion.x;
				mouse_y = e.motion.y;
				break;
		}
	}
	SDL_Delay(2);
}
