#include "core.h"
#include "waiter.h"
#include <SDL2/SDL.h>
#include <algorithm>

int ticks(){
	return SDL_GetTicks();
}

set<Waiter*> Waiter::queue;

bool Waiter::operator<(const Waiter &right){
    return this->time < right.time;
}

Waiter::Waiter(int t, const std::function<void()>& cb) {
    time = ticks()+t;
    callback = cb;
}

void Waiter::remove() {
    auto it = queue.find(this);
    if(it != queue.begin())
        queue.erase(it);
    delete this;
}

Waiter* Waiter::add(int t, const std::function<void()>& cb) {
    Waiter* waiter = new Waiter(t, cb);
    queue.insert(waiter);
    return waiter;
}

void Waiter::check() {
    auto it = queue.begin();
    int t = ticks();
    while(it != queue.end()) {
        if((**it).time < t) {
            (**it).callback();
            Waiter* w = *it;
            queue.erase(it);
            delete w;
        } else break;
    }
}