#pragma once
#include <algorithm>
#include <functional>
#include <mutex>

template <typename... Args>
using const_function = std::function<void(const Args&...)>;

template <typename... Args>
class Event {
 public:
  struct Listener {
    const_function<Args...> cb_;
    Listener(const const_function<Args...>& cb) : cb_(cb) {}
  };

 private:
  std::vector<Listener*> listeners_;

 public:
  Listener* listen(const const_function<Args...>& callback) {
    auto nl = new Listener(callback);
    listeners_.push_back(nl);
    return nl;
  }
  bool unlisten(const Listener* listener) {
    auto it = std::find(listeners_.begin(), listeners_.end(), listener);
    if (it != listeners_.end()) {
      Listener* l = *it;
      listeners_.erase(it);
      delete l;
      return true;
    }
    return false;
  }
  inline void call(const Args&... args) {
    for (auto listener : listeners_)
      if (listener != nullptr) listener->cb_(args...);
  }
};

// This scary thing runs tuple<Args...> into function(Args...)
// And it fucking works
namespace tuple_unpack {
template <int...>
struct indices;

template <int N, typename Indices, typename... Types>
struct make_indices_impl;

template <int N, int... Indices, typename Type, typename... Types>
struct make_indices_impl<N, indices<Indices...>, Type, Types...> {
  typedef
      typename make_indices_impl<N + 1, indices<Indices..., N>, Types...>::type
          type;
};

template <int N, int... Indices>
struct make_indices_impl<N, indices<Indices...>> {
  typedef indices<Indices...> type;
};
template <int N, typename... Types>
struct make_indices {
  typedef typename make_indices_impl<0, indices<>, Types...>::type type;
};

template <typename Indices>
struct apply_tuple_impl;

template <typename Op, typename... OpArgs,
          typename Indices = typename make_indices<0, OpArgs...>::type,
          template <typename...> class T = std::tuple>
auto call(Op&& op, T<OpArgs...> t) ->
    typename std::result_of<Op(OpArgs...)>::type {
  return apply_tuple_impl<Indices>::apply_tuple(std::forward<Op>(op),
                                                std::forward<T<OpArgs...>>(t));
}
template <template <int...> class I, int... Indices>
struct apply_tuple_impl<I<Indices...>> {
  template <typename Op, typename... OpArgs,
            template <typename...> class T = std::tuple>
  static auto apply_tuple(Op&& op, T<OpArgs...>&& t) ->
      typename std::result_of<Op(OpArgs...)>::type {
    return op(std::forward<OpArgs>(std::get<Indices>(t))...);
  }
};
// call(function_name, tuple<Args...>) is translated into function_name(Args...)
// with args from tuple
}  // namespace tuple_unpack

// Supports multithreading - callback is called on Listener::flush()
template <typename... Args>
class ExternEvent {
  class Listener {
    std::mutex mutex_;
    const_function<Args...> callback_;
    std::vector<std::tuple<Args...>> calls_;
    Event<Args...>& ev_;
    typename Event<Args...>::Listener* ev_listener_;

   public:
    ~Listener() { ev_.unlisten(ev_listener_); }
    Listener(Event<Args...>& ev, const const_function<Args...>& callback)
        : callback_(callback), ev_(ev) {
      ev_listener_ = ev.listen([this](Args... args) {
        mutex_.lock();
        calls_.emplace_back(args...);
        mutex_.unlock();
      });
    }
    void flush() {
      mutex_.lock();
      for (auto& i : calls_) {
        // constexpr auto cv = CvValue<std::decay_t<T>>::value;
        tuple_unpack::call(callback_, i);
      }
      calls_.erase(calls_.begin(), calls_.end());
      mutex_.unlock();
    }
  };
  std::vector<Listener*> listeners_;
  Event<Args...> event_;

 public:
  // Remember to save returned Listener and flush it then repeatedly
  Listener* listen(const std::function<void(Args...)>& callback) {
    auto l = new Listener(event_, callback);
    listeners_.emplace_back(l);
    return l;
  }
  bool unlisten(const Listener* listener) {
    auto f = std::find(listeners_.begin(), listeners_.end(), listener);
    if (f != listeners_.end()) {
      listeners_.erase(f);
      return true;
    }
    return false;
  }
  inline void call(Args... args) { event_.call(args...); }
};