#pragma once
#include <chrono>
#include <string>

class StopWatch {
    std::chrono::high_resolution_clock::time_point start_time;
public:
    // initializes start_time with 'now'
    StopWatch();

    // updates start_time with 'now'
    void update();
    // returns time elapsed in nanoseconds since 'this' construction in nanoseconds
    long stop_watch() const;
    // returns time between given StopWatch and 'this' construction in nanoseconds
    long diff(const StopWatch& time_point) const;
    // do /1e3 or /1e6 for micro- and milliseconds if needed
    
    // returns current time in format of current locale
    static std::string getTimeString();
};