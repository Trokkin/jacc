#pragma once
#include "stop_watch.h"
#include <fstream>
#include <string>
#include <cstdarg>
#include <mutex>

enum LogLevel {
    ERROR, WARNING, INFO, DEBUG, TRACE
};

class Log {
	FILE* stream_;
	LogLevel level_ = INFO;
	StopWatch watch_;
	std::mutex mtx_;
	bool redirect_ = false;
	void print(const char*, va_list);
public:
	Log(const std::string& filename);
	Log(const std::string& filename, LogLevel);

	inline void setLevel(LogLevel lvl) { level_ = lvl; };
	inline LogLevel getLevel() const { return level_; };

	void msg(LogLevel, const char *, ...);
	void msg(const char *, ...);

	inline void redirect(bool flag) { redirect_ = flag;}
};

extern Log mlog;

std::string format(const char* fmt, ...);