#pragma once
#include <vector>
#include "event.h"
using namespace std;

extern Event<> event_always;
extern Event<int, int> event_lclick;

class Hex;

class Entity {
protected:
	Hex* place;
	void move_to(Hex* to_where);
};

class Hex{
public:
	int owner;
	vector<Entity*> contains;
};

class Layer {
public:
	int width, height;
	Hex** hexes;
};

class Player {
public:
	
};

class Session {
public:
	long time;
	Layer map;
	vector<Player> players;	
};

void create_session();
void close_session();
extern Session* current_session;

extern int run;