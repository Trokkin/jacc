#pragma once
#include "interface.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>
using namespace std;


void init_graphics();
void freeGraphics();

class Manager{
public:
	vector<SDL_Texture*> textures;
};

class Sprite{
public:
	Sprite(){}
	Sprite(char* path);
	SDL_Texture *tex;
};

extern SDL_Renderer *rend;
extern SDL_Window *win;

extern Manager manager;