#pragma once
#include <functional>
#include <set>

int ticks();

class Waiter {
    static set<Waiter*> queue;
    int time;
    std::function<void()> callback;
    Waiter(int t, const std::function<void()>& cb);
public:
	bool operator<(const Waiter &right);
    void remove();
    inline int getTimeRemaining() const { return time - ticks(); };
    static Waiter* add(int t, const std::function<void()>& cb);
    static void check();
};