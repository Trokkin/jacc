#include "interface.h"
#include "graphics.h"
#include "core.h"
#include "waiter.h"
#include "event.h"
#include "logger.h"
#include <iostream>
using namespace std;

int main(int argc,char **argv){
	create_session();
	init_graphics();
	event_lclick.listen([](int x, int y){
		cout<<x<<' '<<y<<endl;
		Waiter::add(3000, [](){ cout<<"OK"<<endl; });
	});
	
	while(run){
		catch_event();
	}
	
	freeGraphics();
	close_session();
	return 0;
}