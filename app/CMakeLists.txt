project(app)

set(EXECUTABLE  main.cpp)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR})

add_custom_target(run_app
    COMMAND app
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
)

add_executable(app ${EXECUTABLE})
target_link_libraries(app core)
