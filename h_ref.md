# *callback.h*
- `class Callback` : Базовый абстрактный класс для отложенного вызова функции через данное время или на данном событии.
- `void connect(int, func {,args})` : добавление отложенного вызова c указанными данными или "слушателя" какого-либо события.
- `class VoidEvent : public Callback` : для функции без аргументов.
- `class IIEvent : public Callback` : для функции с аргументом  `pair<int,int>`.
- `void connect(int, func)` : перегрузка для VoidEvent
- `void connect(int, func, pair<int,int>)` : перегрузка для IIEvent
- `set<Callback*> queue` : очередь функций, отсортированных по времени вызова в возрастающем порядке.
- `vector<Callback*> listeners[EVENT_NUM]` : все функции, ожидающие каких-либо событий.
- `#define EVENT_NUM` : количество различных типов событий 
- `#define LCLICK` : событие клика левой кнопкой мыши.
- `#define ALWAYS` : событие, происходящее каждый такт движка.
# *core.h*
- `class World` : класс сессии игры.
- `class Hex` : класс одной клетки.
- `class Building` : класс постройки.
- `class Unit` : класс юнита. (??)
- `void init_core()` : инициализирует сессию игры.
- `void free_core()` : закрывает сессию игры.
- `int run` : почти-bool, если игра запущена.
- `World world` : текущая сессия игры.
# *graphics.h*
- `class Sprite` : обертка на SDL_Texture.
- `void init_graphics()` : инициализирует графику.
- `void free_graphics()` : закрывает графику.
- `SDL_Renderer* rend` :
- `SDL_Window* win` :
- `class Manager` ; `Manager manager` : singletone класс-контейнер для всех загруженных текстур.
# *interface.h*
- `#define WIN_W` ; `#define WIN_H` : разрешение окна игры.
- `class Input` ; `Input input` : singletone класс, содержащий данные о состоянии контроллеров (позиция мыши, нажатые клавиши)
- `void activate(int event)` : исполняет указанное событие.
- `int ticks()` : возвращает время, прошедшее с запуска программы в миллисекундах.
- `void catch_event()` : обрабатывает все события, которые получил SDL с предыдущего исполнения этой функции.